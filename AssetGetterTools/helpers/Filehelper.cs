﻿using AssetGetterTools.helpers;
using AssetsTools.NET;
using AssetsTools.NET.Extra;
using Etc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UABEAvalonia;
using uTinyRipper;
using uTinyRipper.Converters;
using uTinyRipper.Layout;
using uTinyRipper.Project;
using uTinyRipper.SerializedFiles;

namespace AssetGetterTools
{
    public class Filehelper
    {
        public string workingFolder { get; set; }

        public void UnpackBundle(string inFile, string targetFolder, string assetName)
        {
            Directory.CreateDirectory(targetFolder);

            var textureExporter = new TextureExporter();

            var pathes = new List<string>();
            pathes.Add(inFile);

            var assetManager = new AssetsManager();

            if (File.Exists("classdata.tpk"))
            {
                assetManager.LoadClassPackage("classdata.tpk");
            }
            else
            {
                throw new Exception("Missing classdata.tpk Please make sure it exists.");
            }


            var bundleInst = assetManager.LoadBundleFile(inFile, false);

            if (AssetBundleUtil.IsBundleDataCompressed(bundleInst.file))
            {
                DecompressToMemory(bundleInst);

                var decompressedBundleNames = new List<string>();

                for (var i = 0; i < bundleInst.file.bundleInf6.dirInf.Length; i++)
                {
                    string bunAssetName = bundleInst.file.bundleInf6.dirInf[i].name;
                    byte[] assetData = BundleHelper.LoadAssetDataFromBundle(bundleInst.file, i);

                    File.WriteAllBytes($"{workingFolder}/tmp/{bunAssetName}", assetData);

                    if (!bunAssetName.ToLower().EndsWith(".ress"))
                    {
                        decompressedBundleNames.Add($"{workingFolder}/tmp/{bunAssetName}");
                    }
                }

                var gameStructure = GameStructure.Load(decompressedBundleNames);
                var files = gameStructure.FileCollection.FetchSerializedFiles();

                var collections = new List<IExportCollection>();
                var depList = new List<uTinyRipper.Classes.Object>();
                var depSet = new HashSet<uTinyRipper.Classes.Object>();
                var queued = new HashSet<uTinyRipper.Classes.Object>();
                var options = new ExportOptions(uTinyRipper.Version.MinVersion, Platform.Android, TransferInstructionFlags.AllowTextSerialization);

                var info = new LayoutInfo(options.Version, options.Platform, options.Flags);
                var exportLayout = new AssetLayout(info);
                var virtualFile = new VirtualSerializedFile(exportLayout);

                foreach (var file in files)
                {
                    foreach (var asset in file.FetchAssets())
                    {
                        if (!options.Filter(asset))
                        {
                            continue;
                        }

                        depList.Add(asset);
                        depSet.Add(asset);
                    }
                }

                for (int i = 0; i < depList.Count; i++)
                {
                    uTinyRipper.Classes.Object asset = depList[i];
                    if (!queued.Contains(asset))
                    {
                        IExportCollection collection = textureExporter.CreateCollection(virtualFile, asset, options);
                        foreach (var element in collection.Assets)
                        {
                            queued.Add(element);
                        }
                        collections.Add(collection);
                    }
                }

                foreach (var collect in collections)
                {
                    var all2DAssets = collect.Assets.Where(asset => asset.AssetInfo.ClassID == ClassIDType.Texture2D);

                    foreach (var textureAsset in all2DAssets)
                    {
                        textureExporter.ExportTexture(textureAsset, targetFolder);
                    }
                }
            }
        }

        private void DecompressToMemory(BundleFileInstance bundleInst)
        {
            AssetBundleFile bundle = bundleInst.file;

            MemoryStream bundleStream = new MemoryStream();
            bundle.Unpack(bundle.reader, new AssetsFileWriter(bundleStream));

            bundleStream.Position = 0;

            AssetBundleFile newBundle = new AssetBundleFile();
            newBundle.Read(new AssetsFileReader(bundleStream), false);

            bundle.reader.Close();
            bundleInst.file = newBundle;
        }

    }
}
