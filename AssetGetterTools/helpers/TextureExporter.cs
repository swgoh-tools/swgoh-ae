﻿using Etc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using uTinyRipper;
using uTinyRipper.Classes;
using uTinyRipper.Classes.Textures;
using uTinyRipper.Converters;
using uTinyRipper.Project;
using uTinyRipper.SerializedFiles;

namespace AssetGetterTools.helpers
{
    public class TextureExporter
	{
		private readonly Dictionary<uTinyRipper.ClassIDType, Stack<IAssetExporter>> m_exporters = new Dictionary<uTinyRipper.ClassIDType, Stack<IAssetExporter>>();
		private YAMLAssetExporter YamlExporter { get; } = new YAMLAssetExporter();
		private BinaryAssetExporter BinExporter { get; } = new BinaryAssetExporter();
		private ScriptAssetExporter ScriptExporter { get; } = new ScriptAssetExporter();
		private myDummyExporter DummyExporter { get; } = new myDummyExporter();
		private IFileCollection m_fileCollection;

		public TextureExporter()
        {
			InitializeExporters(null);
		}


		public void OverrideExporter(uTinyRipper.ClassIDType classType, IAssetExporter exporter)
		{
			if (exporter == null)
			{
				throw new ArgumentNullException(nameof(exporter));
			}
			if (!m_exporters.ContainsKey(classType))
			{
				m_exporters[classType] = new Stack<IAssetExporter>(2);

			}
			m_exporters[classType].Push(exporter);
		}

		public void OverrideDummyExporter(ClassIDType classType, bool isEmptyCollection, bool isMetaType)
		{
			DummyExporter.SetUpClassType(classType, isEmptyCollection, isMetaType);
			OverrideExporter(classType, DummyExporter);
		}

		public void OverrideYamlExporter(ClassIDType classType)
		{
			OverrideExporter(classType, YamlExporter);
		}

		public void OverrideBinaryExporter(ClassIDType classType)
		{
			OverrideExporter(classType, BinExporter);
		}

		private void InitializeExporters(IFileCollection fileCollection)
		{
			m_fileCollection = fileCollection;

			OverrideDummyExporter(ClassIDType.MonoManager, true, false);
			OverrideDummyExporter(ClassIDType.BuildSettings, false, false);
			OverrideDummyExporter(ClassIDType.AssetBundle, true, false);
			OverrideDummyExporter(ClassIDType.ResourceManager, true, false);
			OverrideDummyExporter(ClassIDType.PreloadData, true, false);
			OverrideDummyExporter(ClassIDType.EditorSettings, false, false);
			OverrideDummyExporter(ClassIDType.Sprite, false, true);
			OverrideDummyExporter(ClassIDType.TextureImporter, false, false);
			OverrideDummyExporter(ClassIDType.DefaultAsset, false, false);
			OverrideDummyExporter(ClassIDType.DefaultImporter, false, false);
			OverrideDummyExporter(ClassIDType.NativeFormatImporter, false, false);
			OverrideDummyExporter(ClassIDType.MonoImporter, false, false);
			OverrideDummyExporter(ClassIDType.DDSImporter, false, false);
			OverrideDummyExporter(ClassIDType.PVRImporter, false, false);
			OverrideDummyExporter(ClassIDType.ASTCImporter, false, false);
			OverrideDummyExporter(ClassIDType.KTXImporter, false, false);
			OverrideDummyExporter(ClassIDType.IHVImageFormatImporter, false, false);
			OverrideDummyExporter(ClassIDType.SpriteAtlas, false, false);

			OverrideYamlExporter(ClassIDType.GameObject);
			OverrideYamlExporter(ClassIDType.Transform);
			OverrideYamlExporter(ClassIDType.TimeManager);
			OverrideYamlExporter(ClassIDType.AudioManager);
			OverrideYamlExporter(ClassIDType.InputManager);
			OverrideYamlExporter(ClassIDType.Physics2DSettings);
			OverrideYamlExporter(ClassIDType.Camera);
			OverrideYamlExporter(ClassIDType.Material);
			OverrideYamlExporter(ClassIDType.MeshRenderer);
			OverrideYamlExporter(ClassIDType.Texture2D);
			OverrideYamlExporter(ClassIDType.OcclusionCullingSettings);
			OverrideYamlExporter(ClassIDType.GraphicsSettings);
			OverrideYamlExporter(ClassIDType.MeshFilter);
			OverrideYamlExporter(ClassIDType.OcclusionPortal);
			OverrideYamlExporter(ClassIDType.Mesh);
			OverrideYamlExporter(ClassIDType.Skybox);
			OverrideYamlExporter(ClassIDType.QualitySettings);
			OverrideYamlExporter(ClassIDType.TextAsset);
			OverrideYamlExporter(ClassIDType.Rigidbody2D);
			OverrideYamlExporter(ClassIDType.Collider2D);
			OverrideYamlExporter(ClassIDType.Rigidbody);
			OverrideYamlExporter(ClassIDType.PhysicsManager);
			OverrideYamlExporter(ClassIDType.CircleCollider2D);
			OverrideYamlExporter(ClassIDType.PolygonCollider2D);
			OverrideYamlExporter(ClassIDType.BoxCollider2D);
			OverrideYamlExporter(ClassIDType.PhysicsMaterial2D);
			OverrideYamlExporter(ClassIDType.MeshCollider);
			OverrideYamlExporter(ClassIDType.BoxCollider);
			OverrideYamlExporter(ClassIDType.CompositeCollider2D);
			OverrideYamlExporter(ClassIDType.EdgeCollider2D);
			OverrideYamlExporter(ClassIDType.CapsuleCollider2D);
			OverrideYamlExporter(ClassIDType.AnimationClip);
			OverrideYamlExporter(ClassIDType.TagManager);
			OverrideYamlExporter(ClassIDType.AudioListener);
			OverrideYamlExporter(ClassIDType.AudioSource);
			OverrideYamlExporter(ClassIDType.RenderTexture);
			OverrideYamlExporter(ClassIDType.Cubemap);
			OverrideYamlExporter(ClassIDType.Avatar);
			OverrideYamlExporter(ClassIDType.AnimatorController);
			OverrideYamlExporter(ClassIDType.GUILayer);
			OverrideYamlExporter(ClassIDType.Animator);
			OverrideYamlExporter(ClassIDType.TextMesh);
			OverrideYamlExporter(ClassIDType.RenderSettings);
			OverrideYamlExporter(ClassIDType.Light);
			OverrideYamlExporter(ClassIDType.Animation);
			OverrideYamlExporter(ClassIDType.TrailRenderer);
			OverrideYamlExporter(ClassIDType.MonoBehaviour);
			OverrideYamlExporter(ClassIDType.Texture3D);
			OverrideYamlExporter(ClassIDType.NewAnimationTrack);
			OverrideYamlExporter(ClassIDType.FlareLayer);
			OverrideYamlExporter(ClassIDType.NavMeshProjectSettings);
			OverrideYamlExporter(ClassIDType.Font);
			OverrideYamlExporter(ClassIDType.GUITexture);
			OverrideYamlExporter(ClassIDType.GUIText);
			OverrideYamlExporter(ClassIDType.PhysicMaterial);
			OverrideYamlExporter(ClassIDType.SphereCollider);
			OverrideYamlExporter(ClassIDType.CapsuleCollider);
			OverrideYamlExporter(ClassIDType.SkinnedMeshRenderer);
			OverrideYamlExporter(ClassIDType.BuildSettings);
			OverrideYamlExporter(ClassIDType.CharacterController);
			OverrideYamlExporter(ClassIDType.WheelCollider);
			OverrideYamlExporter(ClassIDType.NetworkManager);
			OverrideYamlExporter(ClassIDType.MovieTexture);
			OverrideYamlExporter(ClassIDType.TerrainCollider);
			OverrideYamlExporter(ClassIDType.TerrainData);
			OverrideYamlExporter(ClassIDType.LightmapSettings);
			OverrideYamlExporter(ClassIDType.AudioReverbZone);
			OverrideYamlExporter(ClassIDType.OffMeshLink);
			OverrideYamlExporter(ClassIDType.OcclusionArea);
			OverrideYamlExporter(ClassIDType.NavMeshObsolete);
			OverrideYamlExporter(ClassIDType.NavMeshAgent);
			OverrideYamlExporter(ClassIDType.NavMeshSettings);
			OverrideYamlExporter(ClassIDType.ParticleSystem);
			OverrideYamlExporter(ClassIDType.ParticleSystemRenderer);
			OverrideYamlExporter(ClassIDType.ShaderVariantCollection);
			OverrideYamlExporter(ClassIDType.LODGroup);
			OverrideYamlExporter(ClassIDType.NavMeshObstacle);
			OverrideYamlExporter(ClassIDType.SortingGroup);
			OverrideYamlExporter(ClassIDType.SpriteRenderer);
			OverrideYamlExporter(ClassIDType.ReflectionProbe);
			OverrideYamlExporter(ClassIDType.Terrain);
			OverrideYamlExporter(ClassIDType.AnimatorOverrideController);
			OverrideYamlExporter(ClassIDType.CanvasRenderer);
			OverrideYamlExporter(ClassIDType.Canvas);
			OverrideYamlExporter(ClassIDType.RectTransform);
			OverrideYamlExporter(ClassIDType.CanvasGroup);
			OverrideYamlExporter(ClassIDType.ClusterInputManager);
			OverrideYamlExporter(ClassIDType.NavMeshData);
			OverrideYamlExporter(ClassIDType.UnityConnectSettings);
			OverrideYamlExporter(ClassIDType.AvatarMask1);
			OverrideYamlExporter(ClassIDType.ParticleSystemForceField);
			OverrideYamlExporter(ClassIDType.OcclusionCullingData);
			OverrideYamlExporter(ClassIDType.PrefabInstance);
			OverrideYamlExporter(ClassIDType.AvatarMask);
			OverrideYamlExporter(ClassIDType.SceneAsset);
			OverrideYamlExporter(ClassIDType.LightmapParameters);
			OverrideYamlExporter(ClassIDType.SpriteAtlas);
			OverrideYamlExporter(ClassIDType.TerrainLayer);

			OverrideBinaryExporter(ClassIDType.Shader);
			OverrideBinaryExporter(ClassIDType.AudioClip);

			OverrideExporter(ClassIDType.MonoScript, ScriptExporter);
		}

		public bool ExportTexture(uTinyRipper.Classes.Object textureAsset, string targetPath)
		{
			Texture2D texture = (Texture2D)textureAsset;

			if (!texture.CheckAssetIntegrity())
			{
				Console.WriteLine($"Can't export '{texture.Name}' because resources file '{texture.StreamData.Path}' hasn't been found");
				return false;
			}

			//do exporting
			return ExportTexture(texture, targetPath);

		}

		public bool ExportTexture(Texture2D texture, string targetPath)
		{
			byte[] buffer = (byte[])texture.GetImageData();
			if (buffer.Length == 0)
			{
				return false;
			}

			var bitMap = ConvertToBitmap(texture, buffer);

			var outFilePath = $"{targetPath}/{texture.Name}.png";
			bitMap.Save(outFilePath, ImageFormat.Png);

			return true;
		}

		private static Bitmap ConvertToBitmap(Texture2D texture, byte[] data)
		{
			switch (texture.TextureFormat)
			{
				case TextureFormat.DXT1:
				case TextureFormat.DXT3:
				case TextureFormat.DXT5:
					return BarbTextureConverter.DXTTextureToBitmap(texture, data);

				case TextureFormat.Alpha8:
				case TextureFormat.ARGB4444:
				case TextureFormat.RGB24:
				case TextureFormat.RGBA32:
				case TextureFormat.ARGB32:
				case TextureFormat.RGB565:
				case TextureFormat.R16:
				case TextureFormat.RGBA4444:
				case TextureFormat.BGRA32:
				case TextureFormat.RHalf:
				case TextureFormat.RGHalf:
				case TextureFormat.RGBAHalf:
				case TextureFormat.RFloat:
				case TextureFormat.RGFloat:
				case TextureFormat.RGBAFloat:
				case TextureFormat.RGB9e5Float:
				case TextureFormat.RG16:
				case TextureFormat.R8:
					return BarbTextureConverter.RGBTextureToBitmap(texture, data);

				case TextureFormat.YUY2:
					return BarbTextureConverter.YUY2TextureToBitmap(texture, data);

				case TextureFormat.PVRTC_RGB2:
				case TextureFormat.PVRTC_RGBA2:
				case TextureFormat.PVRTC_RGB4:
				case TextureFormat.PVRTC_RGBA4:
					return BarbTextureConverter.PVRTCTextureToBitmap(texture, data);

				case TextureFormat.ETC_RGB4:
				case TextureFormat.EAC_R:
				case TextureFormat.EAC_R_SIGNED:
				case TextureFormat.EAC_RG:
				case TextureFormat.EAC_RG_SIGNED:
				case TextureFormat.ETC2_RGB:
				case TextureFormat.ETC2_RGBA1:
				case TextureFormat.ETC2_RGBA8:
				case TextureFormat.ETC_RGB4_3DS:
				case TextureFormat.ETC_RGBA8_3DS:
					return BarbTextureConverter.ETCTextureToBitmap(texture, data);

				case TextureFormat.ATC_RGB4:
				case TextureFormat.ATC_RGBA8:
					return BarbTextureConverter.ATCTextureToBitmap(texture, data);

				case TextureFormat.ASTC_RGB_4x4:
				case TextureFormat.ASTC_RGB_5x5:
				case TextureFormat.ASTC_RGB_6x6:
				case TextureFormat.ASTC_RGB_8x8:
				case TextureFormat.ASTC_RGB_10x10:
				case TextureFormat.ASTC_RGB_12x12:
				case TextureFormat.ASTC_RGBA_4x4:
				case TextureFormat.ASTC_RGBA_5x5:
				case TextureFormat.ASTC_RGBA_6x6:
				case TextureFormat.ASTC_RGBA_8x8:
				case TextureFormat.ASTC_RGBA_10x10:
				case TextureFormat.ASTC_RGBA_12x12:
					return BarbTextureConverter.ASTCTextureToBitmap(texture, data);

				case TextureFormat.BC4:
				case TextureFormat.BC5:
				case TextureFormat.BC6H:
				case TextureFormat.BC7:
					return BarbTextureConverter.TexgenpackTextureToBitmap(texture, data);

				case TextureFormat.DXT1Crunched:
				case TextureFormat.DXT5Crunched:
					return BarbTextureConverter.DXTCrunchedTextureToBitmap(texture, data);

				case TextureFormat.ETC_RGB4Crunched:
				case TextureFormat.ETC2_RGBA8Crunched:
					return BarbTextureConverter.ETCCrunchedTextureToBitmap(texture, data);

				default:
					Console.WriteLine($"Unsupported texture format '{texture.TextureFormat}'");
					return null;
			}
		}

		public IExportCollection CreateCollection(VirtualSerializedFile file, uTinyRipper.Classes.Object asset, ExportOptions options)
		{
			Stack<IAssetExporter> exporters = m_exporters[asset.ClassID];
			foreach (IAssetExporter exporter in exporters)
			{
				if (exporter.IsHandle(asset, options))
				{
					return exporter.CreateCollection(file, asset);
				}
			}
			throw new Exception($"There is no exporter that can handle '{asset}'");
		}
	}
}
