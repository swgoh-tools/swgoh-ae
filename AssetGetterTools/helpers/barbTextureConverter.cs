﻿using Astc;
using Atc;
using Dxt;
using Etc;
using Pvrtc;
using Rgb;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Text;
using uTinyRipper.Classes;
using uTinyRipper.Classes.Textures;
using uTinyRipperGUI.Exporters;
using Yuy2;

namespace AssetGetterTools.helpers
{
    public class BarbTextureConverter
    {
        public static Bitmap ETCTextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            byte[] outbytes = new byte[width * height * 4];

            switch (texture.TextureFormat)
            {
                case TextureFormat.ETC_RGB4:
                case TextureFormat.ETC_RGB4_3DS:
                case TextureFormat.ETC_RGB4Crunched:
                    EtcDecoder.DecompressETC(data, width, height, outbytes);
                    break;

                case TextureFormat.EAC_R:
                    EtcDecoder.DecompressEACRUnsigned(data, width, height, outbytes);
                    break;
                case TextureFormat.EAC_R_SIGNED:
                    EtcDecoder.DecompressEACRSigned(data, width, height, outbytes);
                    break;
                case TextureFormat.EAC_RG:
                    EtcDecoder.DecompressEACRGUnsigned(data, width, height, outbytes);
                    break;
                case TextureFormat.EAC_RG_SIGNED:
                    EtcDecoder.DecompressEACRGSigned(data, width, height, outbytes);
                    break;

                case TextureFormat.ETC2_RGB:
                    EtcDecoder.DecompressETC2(data, width, height, outbytes);
                    break;

                case TextureFormat.ETC2_RGBA1:
                    EtcDecoder.DecompressETC2A1(data, width, height, outbytes);
                    break;

                case TextureFormat.ETC2_RGBA8:
                case TextureFormat.ETC_RGBA8_3DS:
                case TextureFormat.ETC2_RGBA8Crunched:
                    EtcDecoder.DecompressETC2A8(data, width, height, outbytes);
                    break;

                default:
                    throw new Exception(texture.TextureFormat.ToString());

            }

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;

        }

        public static Bitmap DXTTextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            byte[] outbytes = new byte[width * height * 4];

            switch (texture.TextureFormat)
            {
                case TextureFormat.DXT1:
                case TextureFormat.DXT1Crunched:
                    DxtDecoder.DecompressDXT1(data, width, height, outbytes);
                    break;

                case TextureFormat.DXT3:
                    DxtDecoder.DecompressDXT3(data, width, height, outbytes);
                    break;

                case TextureFormat.DXT5:
                case TextureFormat.DXT5Crunched:
                    DxtDecoder.DecompressDXT5(data, width, height, outbytes);
                    break;

                default:
                    throw new Exception(texture.TextureFormat.ToString());
            }

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;
        }

        public static Bitmap ATCTextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            byte[] outbytes = new byte[width * height * 4];

            switch (texture.TextureFormat)
            {
                case TextureFormat.ATC_RGB4:
                    AtcDecoder.DecompressAtcRgb4(data, width, height, outbytes);
                    break;

                case TextureFormat.ATC_RGBA8:
                    AtcDecoder.DecompressAtcRgba8(data, width, height, outbytes);
                    break;

                default:
                    throw new Exception(texture.TextureFormat.ToString());
            }

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;
        }

        public static Bitmap ASTCTextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            int blockSize = texture.ASTCBlockSize();
            byte[] outbytes = new byte[width * height * 4];
            AstcDecoder.DecodeASTC(data, width, height, blockSize, blockSize, outbytes);

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;
        }

        public static Bitmap TexgenpackTextureToBitmap(Texture2D texture, byte[] data)
        {
            //needs noncompatible DLL
            throw new NotImplementedException();
        }

        public static Bitmap DXTCrunchedTextureToBitmap(Texture2D texture, byte[] data)
        {
            //needs noncompatible DLL
            throw new NotImplementedException();
        }

        public static Bitmap ETCCrunchedTextureToBitmap(Texture2D texture, byte[] data)
        {
            //needs noncompatible DLL
            throw new NotImplementedException();
        }

        public static Bitmap PVRTCTextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            byte[] outbytes = new byte[width * height * 4];

            int bitCount = texture.PVRTCBitCount();
            PvrtcDecoder.DecompressPVRTC(data, width, height, outbytes, bitCount == 2);

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;
        }

        public static Bitmap YUY2TextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            byte[] outbytes = new byte[width * height * 4];

            Yuy2Decoder.DecompressYUY2(data, width, height, outbytes);

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;
        }

        public static Bitmap RGBTextureToBitmap(Texture2D texture, byte[] data)
        {
            int width = texture.Width;
            int height = texture.Height;
            byte[] outbytes = new byte[width * height * 4];

            switch (texture.TextureFormat)
            {
                case TextureFormat.Alpha8:
                    RgbConverter.A8ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.ARGB4444:
                    RgbConverter.ARGB16ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGB24:
                    RgbConverter.RGB24ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGBA32:
                    RgbConverter.RGBA32ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.ARGB32:
                    RgbConverter.ARGB32ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGB565:
                    RgbConverter.RGB16ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.R16:
                    RgbConverter.R16ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGBA4444:
                    RgbConverter.RGBA16ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.BGRA32:
                    Buffer.BlockCopy(data, 0, outbytes, 0, outbytes.Length);
                    break;
                case TextureFormat.RG16:
                    RgbConverter.RG16ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.R8:
                    RgbConverter.R8ToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RHalf:
                    RgbConverter.RHalfToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGHalf:
                    RgbConverter.RGHalfToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGBAHalf:
                    RgbConverter.RGBAHalfToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RFloat:
                    RgbConverter.RFloatToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGFloat:
                    RgbConverter.RGFloatToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGBAFloat:
                    RgbConverter.RGBAFloatToBGRA32(data, width, height, outbytes);
                    break;
                case TextureFormat.RGB9e5Float:
                    RgbConverter.RGB9e5FloatToBGRA32(data, width, height, outbytes);
                    break;

                default:
                    throw new Exception(texture.TextureFormat.ToString());
            }

            var m_bitsHandle = GCHandle.Alloc(outbytes, GCHandleType.Pinned);
            var bitMap = new Bitmap(width, height, width * 4, PixelFormat.Format32bppArgb, m_bitsHandle.AddrOfPinnedObject());
            bitMap.RotateFlip(RotateFlipType.Rotate180FlipX);
            return bitMap;
        }

    }
}
