using System;
using uTinyRipper.Classes;
using uTinyRipper.Classes.Textures;

namespace uTinyRipperGUI.Exporters
{
	public static class Texture2DExtensions
	{
		public static bool DDSIsPitchOrLinearSize(this Texture2D _this)
		{
			if (_this.MipCount > 1)
			{
				switch (_this.TextureFormat)
				{
					case TextureFormat.DXT1:
					case TextureFormat.DXT1Crunched:
					case TextureFormat.DXT3:
					case TextureFormat.DXT5:
					case TextureFormat.DXT5Crunched:
						return true;
				}
			}
			return false;
		}

		public static uint DDSFourCC(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.DXT1:
				case TextureFormat.DXT1Crunched:
					// ASCII - 'DXT1'
					return 0x31545844;
				case TextureFormat.DXT3:
					// ASCII - 'DXT3'
					return 0x33545844;
				case TextureFormat.DXT5:
				case TextureFormat.DXT5Crunched:
					// ASCII - 'DXT5'
					return 0x35545844;

				default:
					return 0;
			}
		}

		public static int DDSRGBBitCount(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.RGBA32:
					return 32;
				case TextureFormat.ARGB32:
					return 32;
				case TextureFormat.BGRA32:
					return 32;

				case TextureFormat.ARGB4444:
					return 16;
				case TextureFormat.RGBA4444:
					return 16;

				case TextureFormat.RGB24:
					return 24;

				case TextureFormat.R8:
				case TextureFormat.Alpha8:
					return 8;

				case TextureFormat.R16:
				case TextureFormat.RG16:
					return 16;

				case TextureFormat.RGB565:
					return 16;

				case TextureFormat.DXT1:
				case TextureFormat.DXT1Crunched:
				case TextureFormat.DXT3:
				case TextureFormat.DXT5:
				case TextureFormat.DXT5Crunched:
					return 0;

				default:
					throw new NotSupportedException($"Texture format {_this.TextureFormat} isn't supported");
			}
		}

		public static uint DDSRBitMask(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.RGBA32:
					return 0x000000FF;
				case TextureFormat.BGRA32:
					return 0x00FF0000;
				case TextureFormat.ARGB32:
					return 0x0000FF00;

				case TextureFormat.ARGB4444:
					return 0x0F00;
				case TextureFormat.RGBA4444:
					return 0xF000;

				case TextureFormat.RGB24:
					return 0x0000FF;

				case TextureFormat.RGB565:
					return 0xF800;

				case TextureFormat.Alpha8:
					return 0;

				case TextureFormat.R8:
					return 0xFF;
				case TextureFormat.R16:
					return 0xFFFF;
				case TextureFormat.RG16:
					return 0xFF;

				case TextureFormat.DXT1:
				case TextureFormat.DXT1Crunched:
				case TextureFormat.DXT3:
				case TextureFormat.DXT5:
				case TextureFormat.DXT5Crunched:
					return 0;

				default:
					throw new NotSupportedException($"Texture format {_this.TextureFormat} isn't supported");
			}
		}

		public static uint DDSGBitMask(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.RGBA32:
					return 0x0000FF00;
				case TextureFormat.BGRA32:
					return 0x0000FF00;
				case TextureFormat.ARGB32:
					return 0x00FF0000;

				case TextureFormat.ARGB4444:
					return 0x00F0;
				case TextureFormat.RGBA4444:
					return 0x0F00;

				case TextureFormat.RGB24:
					return 0xFF00;

				case TextureFormat.RGB565:
					return 0x07E0;

				case TextureFormat.Alpha8:

					return 0;
				case TextureFormat.R8:
					return 0;
				case TextureFormat.R16:
					return 0;
				case TextureFormat.RG16:
					return 0xFF00;

				case TextureFormat.DXT1:
				case TextureFormat.DXT1Crunched:
				case TextureFormat.DXT3:
				case TextureFormat.DXT5:
				case TextureFormat.DXT5Crunched:
					return 0;

				default:
					throw new NotSupportedException($"Texture format {_this.TextureFormat} isn't supported");
			}
		}

		public static uint DDSBBitMask(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.RGBA32:
					return 0x00FF0000;
				case TextureFormat.BGRA32:
					return 0x000000FF;
				case TextureFormat.ARGB32:
					return 0xFF000000;

				case TextureFormat.ARGB4444:
					return 0x000F;
				case TextureFormat.RGBA4444:
					return 0x00F0;

				case TextureFormat.RGB24:
					return 0xFF0000;

				case TextureFormat.RGB565:
					return 0x001F;

				case TextureFormat.Alpha8:
					return 0;

				case TextureFormat.R8:
					return 0;
				case TextureFormat.R16:
					return 0;
				case TextureFormat.RG16:
					return 0;

				case TextureFormat.DXT1:
				case TextureFormat.DXT1Crunched:
				case TextureFormat.DXT3:
				case TextureFormat.DXT5:
				case TextureFormat.DXT5Crunched:
					return 0;

				default:
					throw new NotSupportedException($"Texture format {_this.TextureFormat} isn't supported");
			}
		}

		public static uint DDSABitMask(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.RGBA32:
					return 0xFF000000;
				case TextureFormat.BGRA32:
					return 0xFF000000;

				case TextureFormat.ARGB32:
					return 0xFF;
				case TextureFormat.Alpha8:
					return 0xFF;

				case TextureFormat.ARGB4444:
					return 0xF000;
				case TextureFormat.RGBA4444:
					return 0x000F;

				case TextureFormat.RGB24:
					return 0x0;

				case TextureFormat.R8:
					return 0;
				case TextureFormat.R16:
					return 0;
				case TextureFormat.RGB565:
					return 0;
				case TextureFormat.RG16:
					return 0;
				case TextureFormat.DXT1:
				case TextureFormat.DXT1Crunched:
				case TextureFormat.DXT3:
				case TextureFormat.DXT5:
				case TextureFormat.DXT5Crunched:
					return 0;

				default:
					throw new NotSupportedException($"Texture format {_this.TextureFormat} isn't supported");
			}
		}

		public static int PVRTCBitCount(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.PVRTC_RGB2:
				case TextureFormat.PVRTC_RGBA2:
					return 2;

				case TextureFormat.PVRTC_RGB4:
				case TextureFormat.PVRTC_RGBA4:
					return 4;

				default:
					throw new NotSupportedException(_this.TextureFormat.ToString());
			}
		}

		public static int ASTCBlockSize(this Texture2D _this)
		{
			switch (_this.TextureFormat)
			{
				case TextureFormat.ASTC_RGB_4x4:
				case TextureFormat.ASTC_RGBA_4x4:
					return 4;

				case TextureFormat.ASTC_RGB_5x5:
				case TextureFormat.ASTC_RGBA_5x5:
					return 5;

				case TextureFormat.ASTC_RGB_6x6:
				case TextureFormat.ASTC_RGBA_6x6:
					return 6;

				case TextureFormat.ASTC_RGB_8x8:
				case TextureFormat.ASTC_RGBA_8x8:
					return 8;

				case TextureFormat.ASTC_RGB_10x10:
				case TextureFormat.ASTC_RGBA_10x10:
					return 10;

				case TextureFormat.ASTC_RGB_12x12:
				case TextureFormat.ASTC_RGBA_12x12:
					return 12;

				default:
					throw new NotSupportedException(_this.TextureFormat.ToString());
			}
		}
	}
}
